#! /bin/bash
folder_www=
cd /var/www
tar -czvf www_original_teqip.tar $folder_www
echo "Make a copy of tar file if you are running the backup script for first time. Do not use it if you are running the script for second time."
echo "Are you running the script for first time"
select script_time in yes no
do
    if [[ $script_time == "yes" ]] || [[ $script_time == "no" ]]
    then
        echo -e "\033[0;35mYou have selected $script_time.\033[0m"
        break
    else
        echo -e "\033[0;35mPlease enter 1 for yes and 2 for no.\033[0m"      # If proper choice not selected by user then this message will pop up
    fi
done
if [[ $script_time == yes ]]
then
cp www_original_teqip.tar www_original_teqip_copy.tar
fi
if [ -f /var/www/www_original_teqip.tar ]; 
    then
        size=$(du -h /var/www/www_original_teqip.tar)
        echo -e "\033[0;35mbackup file created with size and name $size.\033[0m"      # Include the variables defined in the related to OS.
    else
        echo -e "\033[0;35mBackup file not created yet.\033[0m"
fi
sed -n "/'formatter'/,+13p" /var/www/$folder_www/config/web.php