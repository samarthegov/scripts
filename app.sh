#! /bin/bash
#For most modern Linux OS systems, the file /etc/os-release is really becoming standard and is getting included in most OS. So inside your Bash script you can just include the file, and you will have access to all variables described here (for example: NAME, VERSION, ...)
locon=$(pwd)
exec > >(tee -a /var/log/scriptapp.log) 2>&1
. $locon/teqip-variables  # Include the variables-file in script because entering token through standard input can be difficult and can cause errors.
if [ -f /etc/os-release ]; 
then
    . /etc/os-release      # Include the variables defined in the related to OS.
else
    echo -e "\033[0;35mYour OS distribution is not Centos dist. Currently we do not provide script support for OS distributions other than Centos7.\033[0m"
    exit
fi
if [[ $ID == centos ]] || [[ $VERSION_ID == 7 ]]   # Check for the version of OS. If not centos 7 advise accordingly.
then
    echo -e "\033[0;35mYour System Version is Centos-7.\033[0m"
    yum_check=$(which yum)
    echo $yum_check
    if [[ $yum_check == *yum* ]]   # Check for the installation of yum repo.
    then
        echo -e "\033[0;35myum is installed in the system.\033[0m"
    else
        echo -e "\033[0;35mInstall Yum repo first in the system.\033[0m"
        exit            # if yum not installed break the script.
    fi
    yum update -y
    yum install epel-release -y
    yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
    yum install yum-utils -y
    yum-config-manager --enable remi-php74
    #yum install yum-versionlock -y
    #curl https://bitbucket.org/devopsricha/scripts/raw/HEAD/versionlock --output versionlock.list
    #mv versionlock.list /etc/yum/pluginconf.d/
    yum install httpd wget php unzip php-mysqlnd php-mbstring php-gd php-intl php-curl php-soap php-xml php-imagick php-csv php-mcrypt php-bcmath php-zip php-opcache php-fpm -y
    sed -i 's/enabled=0/enabled=1/g' /etc/yum.repos.d/remi-php74.repo   # change enabled=0 to enabled=1 in remi-php74.repo file.
    systemctl start httpd
    systemctl status httpd
    systemctl enable httpd
    systemctl start php-fpm
    systemctl enable php-fpm
elif [[ $ID == amzn ]] || [[ $VERSION_ID == 2 ]]
then
    echo -e "\033[0;35mYour System Version is Amazon-Linux-2.\033[0m"
    yum_check=$(which yum)
    echo $yum_check
    if [[ $yum_check == *yum* ]]   # Check for the installation of yum repo.
    then
        echo -e "\033[0;35myum is installed in the system.\033[0m"
    else
        echo -e "\033[0;35mInstall Yum repo first in the system.\033[0m"
        exit            # if yum not installed break the script.
    fi
    yum update -y
    yum install -y amazon-linux-extras
    amazon-linux-extras enable php7.4
    yum clean metadata
    # amazon-linux-extras install epel -y
    yum install httpd wget php unzip php-mysqlnd php-mbstring php-mcrypt php-csv php-gd php-intl php-curl php-soap php-xml php-imagick php-bcmath php-zip php-opcache php-fpm -y
    # amazon-linux-extras install epel
    # yum install php-mcrypt
    # yum install php-csv
    # yum install yum-utils -y
    # yum-config-manager --enable remi-php74
    #yum install yum-versionlock -y
    #curl https://bitbucket.org/devopsricha/scripts/raw/HEAD/versionlock --output versionlock.list
    #mv versionlock.list /etc/yum/pluginconf.d/
    # yum install httpd wget php unzip php-mysqlnd php-mbstring php-gd php-intl php-curl php-soap php-xml php-imagick php-csv php-mcrypt php-bcmath php-zip php-opcache php-fpm -y
    # sed -i 's/enabled=0/enabled=1/g' /etc/yum.repos.d/remi-php74.repo   # change enabled=0 to enabled=1 in remi-php74.repo file.
    systemctl start httpd
    systemctl status httpd
    systemctl enable httpd
    systemctl start php-fpm
    systemctl enable php-fpm
else
    echo -e "\033[0;35mYour OS version is not centos 7 or Amazon Linux2. Currently we do not provide script support for OS distributions other than Centos7 or Amazon Linux2.\033[0m"
    exit
fi
cd /var/www     # go to /var/www for further steps
#rm -rf vendor
rm -rf uims_teqip       # Due to some reason if script is to be run again then existence of this folder would cause the script to break again. If this folder doesnt exist this command wouldnt do anything.
rm -rf /var/www/html/teqip     # Due to some reason if script is to be run again then existence of this folder would cause the script to break again.
#rm -rf composer.json        # same as above
# rm -rf /etc/httpd/conf.d/vhosts
# rm -rf /etc/httpd/conf.d/vhosts/apache.conf     #same as above
#php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"       # installing composer
#php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
#php composer-setup.php      # installing composer
#php -r "unlink('composer-setup.php');"      # installing composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
./composer.phar config --global --auth http-basic.teqip.repo.vcs.samarth.ac.in token $consumer_token_samarth       # configure access token of samarth repman for uims/dependencies.
# ./composer.phar config --global --auth http-basic.testing-proj.repo.repman.io token $consumer_token_repman      # configure access token of my repman for www-uims_teqip and html-teqip packages
#wget https://bitbucket.org/devopsricha/scripts/raw/HEAD/composer.json       # temporary composer.json because composer.phar cant run without composer.json. Also it contains the address of my repman.
# ./composer.phar create-project trial/uims_teqip_updated:dev-master uims_teqip --repository-url=https://testing-proj.repo.repman.io  # download package uims_teqip. It will be downloaded into /var/www/vendor/trial/uims_teqip
#mv vendor/trial/uims_teqip .        # by composer.phar packages are downloaded into vendor/vendorname/packagename folder. Move them to desired location.
#./composer.phar require trial/teqip:dev-master      # similar comments as above

#rm -rf vendor       # we no longer need vendor in /var/www because we already got our packages
#rm -rf composer.json        # we no longer need composer.json in /var/www because we already got our packages
./composer.phar create-project samarth/uims-teqip uims_teqip --repository-url=https://teqip.repo.vcs.samarth.ac.in
mv uims_teqip/web /var/www/html/teqip
cd /var/www/html/teqip
rm -f index.php
rm -f itdebug.php
curl https://bitbucket.org/samarthegov/scripts/raw/HEAD/index.php --output index.php
curl https://bitbucket.org/samarthegov/scripts/raw/HEAD/itdebug.php --output itdebug.php
# rm -rf composer.json        # this composer.json was created temporarily for downloading package through composer.phar. Now we dont need it
# mv rncomposer.json composer.json        # we restore our real composer.json file

# ../composer.phar update     # update command to install all the uims and other required packages.


cd /var/www/uims_teqip/config
# sed -i 's://.*::' db.php        # to remove comments in file, otherwise difficult to use sed command
# sed -i 's://.*::' db_admission.php      # to remove comments in file, otherwise difficult to use sed command
# sed -i 's://.*::' db_nt.php     # to remove comments in file, otherwise difficult to use sed command
# sed -i 's://.*::' db_rec.php        # to remove comments in file, otherwise difficult to use sed command
# sed -i 's://.*::' db_student.php        # to remove comments in file, otherwise difficult to use sed command
# sed -i 's://.*::' db_uims.php       # to remove comments in file, otherwise difficult to use sed command
sed -i "s/\${hostname}/$mysql_dsn/" db.php
sed -i "s/\${dbname}/$dbname/" db.php
sed -i "s/\${masterUsername}/$mysql_username/" db.php
sed -i "s/\${masterPass}/$mysql_password/" db.php
sed -i "s/\${slaveUsername}/$mysql_username/" db.php
sed -i "s/\${slavePass}/$mysql_password/" db.php
sed -i "s/\${slaveDSNs}/['dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname']/" db.php

sed -i "s/\${hostname}/$mysql_dsn/" db_student.php
sed -i "s/\${dbname}/$dbname/" db_student.php
sed -i "s/\${masterUsername}/$mysql_username/" db_student.php
sed -i "s/\${masterPass}/$mysql_password/" db_student.php
sed -i "s/\${slaveUsername}/$mysql_username/" db_student.php
sed -i "s/\${slavePass}/$mysql_password/" db_student.php
sed -i "s/\${slaveDSNs}/['dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname']/" db_student.php


sed -i "s/\${hostname}/$mysql_dsn/" db_uims.php
sed -i "s/\${dbname}/$dbname/" db_uims.php
sed -i "s/\${masterUsername}/$mysql_username/" db_uims.php
sed -i "s/\${masterPass}/$mysql_password/" db_uims.php
sed -i "s/\${slaveUsername}/$mysql_username/" db_uims.php
sed -i "s/\${slavePass}/$mysql_password/" db_uims.php
sed -i "s/\${slaveDSNs}/['dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname']/" db_uims.php


random_string_9=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 25 | head -n 1)
echo $random_string_9
sed -i "s/\${cookieValidationKey}/$random_string_9/" web.php
sed -i "s/\${sentryDsn}/https:\/\/7325a089897e4c0294bac88f413a5690\@o913480.ingest.sentry.io\/5851545/" web.php
sed -i "s/\${sentryTag}/$institute_short_code/" web.php


fwallstat=$(systemctl is-active firewalld)      # store status of firewalld in fwallstat
echo -e "\033[0;35mStatus of firewall is $fwallstat\033[0m"
case "$fwallstat" in
#case 1
    "active") echo -e "\033[0;35mAdding 80 and 443 ports to allowed ports in firewalld\033[0m"       # if firewall active then follow following commands
    firewall-cmd --list-all
    firewall-cmd --zone=public --add-port=80/tcp --permanent
    firewall-cmd --zone=public --add-port=443/tcp --permanent
    firewall-cmd --reload
    firewall-cmd --list-all;;
#case 2
    "inactive") echo -e "\033[0;35mNothing to do\033[0m" ;;         #if firewall inactive nothing to do
esac
mkdir /var/www/html/teqip/assets/
mkdir /var/www/uims_teqip/runtime
chown -R apache:apache /var/www/html/teqip/assets/
chown -R apache:apache /var/www/uims_teqip/runtime
selstat=$(getenforce)       # store status of selinux
echo -e "\033[0;35mStatus of Selinux is $selstat\033[0m"
case "$selstat" in
    #case 1
    "Enforcing") echo -e "\033[0;35mModifying contexts of assets and runtime\033[0m"
        chcon -R -t httpd_sys_rw_content_t /var/www/html/teqip/assets/
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/teqip/assets(/.*)?"
        chcon -R -t httpd_sys_rw_content_t /var/www/uims_teqip/runtime
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/uims_teqip/runtime(/.*)?" 
        setsebool -P httpd_can_network_connect_db 1
        ;;
    #case 2
        "Permissive") echo "Nothing to do" ;;
    #case 3
        "Disabled") echo "Nothing to do" ;;
esac
cd /etc/php-fpm.d
cp www.conf www.conf.dist  # backup in case of any issues
sock_status=$(ls /var/run/php-fpm/ | grep .sock)            #To check whether unix socket already exists or not
if [[ $sock_status == *sock* ]]
then
    echo -e "\033[0;35msock already exists\033[0m"              # sock already exists
    if [[ $sock_status != www.sock ]]       # since we in our httpd apache.conf file have used www.conf as unix sock. Here check whether same name .sock exists or not.
    then
        echo -e "\033[0;35mEither rename the Unix sock in /var/run/php-fpm to www.sock or Change the name of sock file in httpd apache.conf same as that in /var/run/php-fpm\033[0m"
    fi
fi
systemctl stop php-fpm
systemctl status php-fpm
sed -i "/listen = 127.0.0.1/c listen = /var/run/php-fpm/www.sock" www.conf     # Generate sock file.
sed -i "/;listen.owner = nobody/c listen.owner = apache" www.conf
sed -i "/;listen.group = nobody/c listen.group = apache" www.conf
systemctl start php-fpm
systemctl status php-fpm
# echo -e "\033[0;32mDo you want to configure Web server for php-fpm. Please select 1 for Yes and 2 for no.\033[0m"        # This process ahead will download model apache config file.
# select webservchoice in yes no
# do
#     if [[ $webservchoice == "yes" ]] || [[ $webservchoice == "no" ]]
#     then
#         echo -e "\033[0;35mYou have selected $webservchoice choice for web server configuration.\033[0m"
#         break
#     else
#         echo -e "\033[0;35mPlease enter 1 for yes and 2 for no.\033[0m"      # If proper choice not selected by user then this message will pop up
#     fi
# done
# echo $webservchoice
# if [[ $webservchoice == yes ]]
# then
#     cd /etc/httpd/conf.d
#     touch vhosts.conf     # create a file conf.d folder
#     echo "IncludeOptional conf.d/vhosts/*.conf" > vhosts.conf     # Include vhosts folder for configurations
#     mkdir vhosts
#     cd vhosts
#     wget https://bitbucket.org/devopsricha/scripts/raw/HEAD/apache.conf        # This model apache.conf file is already stored at this location
#     systemctl restart httpd
#     echo -e "\033[0;32mDo you want to configure ServerName and ServerAlias. Please select 1 for Yes and 2 for no.\033[0m"        # To configure ServerName and ServerAlias through user Input
#     select servernameconf in yes no
#     do
#         if [[ $servernameconf == yes ]] || [[ $servernameconf == no ]]
#         then
#             echo -e "\033[0;35mYou have selected $servernameconf choice for Server Name and Server Alias configuration.\033[0m"
#             break
#         else
#             echo -e "\033[0;35mPlease enter 1 for yes and 2 for no.\033[0m"
#         fi
#     done
#     if [[ $servernameconf == yes ]]
#     then
#         echo -e "\033[0;32mEnter ServerName.\033[0m"
#         read apache_server_name;        # Read the server name by user
#         echo -e "\033[0;32mEnter ServerAlias.\033[0m"
#         read apache_server_alias;       # Read the server alias by user
#         sed -i "/ServerName/c ServerName $apache_server_name" apache.conf     # Add the servername in apache.conf file
#         sed -i "/ServerAlias/c ServerAlias $apache_server_alias" apache.conf     # Add the serveralias in apache.conf file
#         systemctl restart httpd
#     fi
# fi




echo -e "\033[0;32mDo you want to configure Public facing Alumni Portal. Please select 1 for Yes and 2 for no.\033[0m"        # This process ahead will download model apache config file.
select alumni_portal in yes no
do
    if [[ $alumni_portal == "yes" ]] || [[ $alumni_portal == "no" ]]
    then
        echo -e "\033[0;35mYou have selected $alumni_portal choice for Alumni Portal.\033[0m"
        break
    else
        echo -e "\033[0;35mPlease enter 1 for yes and 2 for no.\033[0m"      # If proper choice not selected by user then this message will pop up
    fi
done
echo $alumni_portal
if [[ $alumni_portal == yes ]]
then
cd /var/www/
rm -rf /var/www/html/teqip_alumni
rm -rf /var/www/uims_alumni
./composer.phar create-project samarth/alumni-teqip uims_alumni --repository-url=https://teqip.repo.vcs.samarth.ac.in
cd /var/www/uims_alumni
cp -R conf/ config
random_string_1=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 25 | head -n 1)
echo $random_string_1
sed -i "s/'Enter your key here'/'$random_string_1'/" config/web.php
mv /var/www/uims_alumni/web /var/www/html/teqip_alumni
#mv alumni /var/www/uims_alumni
cd /var/www/uims_alumni/config
rm -f db.php db_uims.php
cp /var/www/uims_teqip/config/db.php db.php
cp db.php db_uims.php
cd /var/www/html/teqip_alumni
rm -f index.php itdebug.php
curl https://bitbucket.org/samarthegov/scripts/raw/HEAD/index.php --output index.php
curl https://bitbucket.org/samarthegov/scripts/raw/HEAD/itdebug.php --output itdebug.php
sed -i "s/uims_teqip/uims_alumni/g" index.php
sed -i "s/uims_teqip/uims_alumni/g" itdebug.php
mkdir /var/www/html/teqip_alumni/assets/
mkdir /var/www/uims_alumni/runtime
chown -R apache:apache /var/www/html/teqip_alumni/assets/
chown -R apache:apache /var/www/uims_alumni/runtime
selstat=$(getenforce)       # store status of selinux
echo -e "\033[0;35mStatus of Selinux is $selstat\033[0m"
case "$selstat" in
    #case 1
    "Enforcing") echo -e "\033[0;35mModifying contexts of assets and runtime\033[0m"
        chcon -R -t httpd_sys_rw_content_t /var/www/html/teqip_alumni/assets/
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/teqip_alumni/assets(/.*)?"
        chcon -R -t httpd_sys_rw_content_t /var/www/uims_alumni/runtime
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/uims_alumni/runtime(/.*)?"
        setsebool -P httpd_can_network_connect_db 1
        ;;
    #case 2
        "Permissive") echo "Nothing to do" ;;
    #case 3
        "Disabled") echo "Nothing to do" ;;
esac
cd /var/www/uims_teqip
php yii migrate-alumni
fi


echo -e "\033[0;32mDo you want to configure Public facing Endowment Portal. Please select 1 for Yes and 2 for no.\033[0m"        # This process ahead will download model apache config file.
select endowment_portal in yes no
do
    if [[ $endowment_portal == "yes" ]] || [[ $endowment_portal == "no" ]]
    then
        echo -e "\033[0;35mYou have selected $endowment_portal choice for Endowment Portal.\033[0m"
        break
    else
        echo -e "\033[0;35mPlease enter 1 for yes and 2 for no.\033[0m"      # If proper choice not selected by user then this message will pop up
    fi
done
echo $endowment_portal
if [[ $endowment_portal == yes ]]
then
cd /var/www/
rm -rf /var/www/html/teqip_endowment
rm -rf /var/www/uims_endowment
./composer.phar create-project samarth/endowment-teqip uims_endowment --repository-url=https://teqip.repo.vcs.samarth.ac.in
cd /var/www/uims_endowment
cp -R conf/ config
random_string_1=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 25 | head -n 1)
echo $random_string_1
sed -i "s/'Enter your key here'/'$random_string_1'/" config/web.php
mv /var/www/uims_endowment/web /var/www/html/teqip_endowment
#mv alumni /var/www/uims_alumni
cd /var/www/uims_endowment/config
rm -f db.php db_uims.php
cp /var/www/uims_teqip/config/db.php db.php
cp db.php db_uims.php
cd /var/www/html/teqip_endowment
rm -f index.php itdebug.php
curl https://bitbucket.org/samarthegov/scripts/raw/HEAD/index.php --output index.php
curl https://bitbucket.org/samarthegov/scripts/raw/HEAD/itdebug.php --output itdebug.php
sed -i "s/uims_teqip/uims_endowment/g" index.php
sed -i "s/uims_teqip/uims_endowment/g" itdebug.php
mkdir /var/www/html/teqip_endowment/assets/
mkdir /var/www/uims_endowment/runtime
chown -R apache:apache /var/www/html/teqip_endowment/assets/
chown -R apache:apache /var/www/uims_endowment/runtime
selstat=$(getenforce)       # store status of selinux
echo -e "\033[0;35mStatus of Selinux is $selstat\033[0m"
case "$selstat" in
    #case 1
    "Enforcing") echo -e "\033[0;35mModifying contexts of assets and runtime\033[0m"
        chcon -R -t httpd_sys_rw_content_t /var/www/html/teqip_endowment/assets/
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/teqip_endowment/assets(/.*)?"
        chcon -R -t httpd_sys_rw_content_t /var/www/uims_endowment/runtime
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/uims_endowment/runtime(/.*)?" 
        setsebool -P httpd_can_network_connect_db 1
        ;;
    #case 2
        "Permissive") echo "Nothing to do" ;;
    #case 3
        "Disabled") echo "Nothing to do" ;;
esac
cd /var/www/uims_teqip
php yii migrate-endowment
fi


# if [[ $webservchoice == yes ]]
# then
#     site_check=$(curl -Is http://127.0.0.1/index.php/site/login | head -1)
#     echo -e "\033[0;35mSite returned $site_check\033[0m"
# else
    site_check=$(curl -Is http://127.0.0.1/teqip/index.php/site/login | head -1)
    echo -e "\033[0;35mSite returned $site_check\033[0m"
# fi 
if [[ $site_check == *200* ]]   # Check for the installation of yum repo.
then
    echo -e "\033[0;32mHurray! Congratulations your site has become operational.\033[0m"
else
    echo -e "\033[0;35mTry opening the site on your browser.\033[0m"
fi