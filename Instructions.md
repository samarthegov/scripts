# Samarth Self Hosted Server and Database Setup from scratch on New Instance.

## **Database Server Setup**  
1. Login on to your Database Server and go to your home directory.  

2. Download Database Server script using following command.  
`curl https://bitbucket.org/iicsam/scripts/raw/HEAD/db.sh --output db.sh`  

3. Assign executable permission to the script using following command.  
`chmod +x db.sh`
     * *Try "sudo chmod +x db.sh" if the command doesn't work.*

4. Run the script using following command.  
`sudo ./db.sh`
     * *User must have sudo priveleges. If User is root user than no need to use sudo command.*
 

## **Application Server Setup**  
1. Login on to your Application Server and go to your home directory.  

2. Download file named teqip-variables using following command.  
`curl https://bitbucket.org/iicsam/scripts/raw/HEAD/teqip-variables --output teqip-variables`  

3. Open the teqip-variables file in editor of your choice and enter/edit the following parameters according to given instructions given below.
     1. * consumer_token_samarth="Enter your Samarth token here"  
        -> *Enter the Repman token provided by Samarth team here*  
     2. * consumer_token_repman="1b7f700ffe0224af749cfcc7d23504c1787445a13736e122a747d5e917d75bef"  
        -> *Keep this token as it is.*  
     3. * mysql_dsn="localhost"  
        -> *Enter the IP address of your database server here if not on same instance.*  
     4. * dbname="teqip_new"             
        -> *Enter the name of the database setup during DB server setup.*   
     5. * mysql_username="root"  
        -> *Enter the Database Server username. If DB server is on different instance then please do not use root user to connect to database.*
     6. * mysql_password="password"  
        -> *Enter the Password setup for user during DB server setup.* 

2. Download Application Server script using following command.  
`curl https://bitbucket.org/iicsam/scripts/raw/HEAD/app.sh --output app.sh`  

3. Give  executable permission to the script using following command.  
`chmod +x app.sh`              
     * *Try `sudo chmod +x db.sh` if the command doesn't work.*

4. Run the script using following command.  
`sudo ./app.sh`                      
     * *User must have sudo priveleges. If User is root user than no need to use sudo command.*

5. Go to your Web Browser and open the site using IP of the instance or ServerName as applicable.  


